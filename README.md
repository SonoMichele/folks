# Folks [folks]

This mod was left unfinished. It's now being maintained by the A.E.S. server, but do not ask for new features since it's in maintenance only.  
Check the [forum thread](https://forum.minetest.net/viewtopic.php?f=9&t=26121&p=389081#p389081) for more info.

## Texture
The folks_default.png was made by [Zughy](https://content.minetest.net/users/Zughy/)
